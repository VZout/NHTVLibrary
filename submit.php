<!DOCTYPE html>
<html>
	<head>
		<title>Submit Game</title>
		<meta charset="UTF-8">
		<meta name="description" content="Sharing video games made at NHTV.">
		<meta name="keywords" content="Video Games, Games, NHTV, Sharing">
		<meta name="author" content="Viktor Zoutman">
		<script src="js/search.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
		<div class="header">
			<a href="index.php" class="header-title">NHTV Games Library</a>
			<a href="#" class="header-link">Submit</a>
		</div>
		<div class="wrapper">
			<form class="submit-form" action="php/submit_form.php" method="post">
				<a>Title: </a> <br>
				<input name="title"placeholder="Battlefield 1" type="text" required><br>
				<a>Author: </a> <br>
				<input name="author" placeholder="Electronic Arts" type="text" required><br>
				<a>Genre: </a> <br>
				<input name="genre" placeholder="First Person Shooter" type="text" required><br>
				<a>Platform: </a> <br>
				<input name="platform" placeholder="windows, linux" type="text" required><br>
				<a>Preview Image URL: </a> <br>
				<input name="preview_url" placeholder="imgur.com/...." type="url" required><br>
				<a>Direct Download URL: </a> <br>
				<input name="download_url" placeholder="dropbox.com/directdwnload" type="url" required><br>
				<a>Description: </a> <br>				
				<textarea name="desc" class="desc-input" placeholder="This game is epic." type="text" required></textarea><br>
				<input value="Submit Game" type="submit"><br>
			</form>
		</div>
	</body>
</html>

