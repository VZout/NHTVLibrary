<?php
include("php/config.php");

session_start();

$sort_types = array("ID", "DATE", "AUTHOR", "GENRE", "TITLE");
 
$query = "SELECT * FROM game";

if (isset($_GET['search'])) {
	$query .= " WHERE TITLE LIKE '%" . $_GET['search'] . "%'";
}

$sort_css_change = "<script>document.getElementById(\"[SORT_TYPE]\").style.textDecoration = \"underline\";</script>";
if (isset($_GET['sort'])) {
	$found_sort_type = false;
	foreach ($sort_types as &$type) {
		if ($_GET['sort'] == $type) {
			$found_sort_type = true;
			$query .= " ORDER BY " . $type;
			$sort_css_change = str_replace("[SORT_TYPE]", $type, $sort_css_change);
			break;
		}
	}
	if (!$found_sort_type) {
		$sort_css_change = str_replace("[SORT_TYPE]", $sort_types[0], $sort_css_change);
	}
} else {
	$sort_css_change = str_replace("[SORT_TYPE]", $sort_types[0], $sort_css_change);
}
echo $sort_css_change;

$result = mysqli_query($db, $query) or die("Query failed");
$row_count = mysqli_num_rows($result);

$game_box_file = fopen("game-box.html", "r");
$game_box_template = fread($game_box_file, filesize("game-box.html"));

while ($row_users = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$game_box = $game_box_template;

	$game_box = str_replace("[TITLE]", $row_users['TITLE'], $game_box);
	$game_box = str_replace("[AUTHOR]", $row_users['AUTHOR'], $game_box);
	$game_box = str_replace("[GENRE]", $row_users['GENRE'], $game_box);
	$game_box = str_replace("[PLATFORM]", $row_users['PLATFORM'], $game_box);
	$game_box = str_replace("[PREVIEW_IMG_URL]", $row_users['PREVIEW_IMG_URL'], $game_box);
	$game_box = str_replace("[DOWNLOAD_URL]", $row_users['DOWNLOAD_URL'], $game_box);
	$game_box = str_replace("[ID]", $row_users['ID'], $game_box);

	echo $game_box;
}

?>
