<!DOCTYPE html>
<html>
	<head>
		<title>NHTV Games Library</title>
		<meta charset="UTF-8">
		<meta name="description" content="Sharing video games made at NHTV.">
		<meta name="keywords" content="Video Games, Games, NHTV, Sharing">
		<meta name="author" content="Viktor Zoutman">
		<script src="js/search.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	</head>
	<body>
		<div class="header">
			<a href="index.php" class="header-title">NHTV Games Library</a>
			<a href="submit.php" class="header-link">Submit</a>
		</div>
		<div class="sub-header">
			<a class="sort-text">Sort By:<a>
			<a onClick="SetGetParameter('sort','ID')" id="ID" class="sub-header-sort">ID</a>
			<a onClick="SetGetParameter('sort','TITLE')" id="TITLE" class="sub-header-sort">Title</a>
			<a onClick="SetGetParameter('sort','GENRE')" id="GENRE" class="sub-header-sort">Genre</a>
			<a onClick="SetGetParameter('sort','AUTHOR')" id="AUTHOR" class="sub-header-sort">Author</a>
			<a onClick="SetGetParameter('sort','DATE')" id="DATE" class="sub-header-sort">Date</a>

			<form class="search-form" action="javascript:Search()">
				<input class="sub-header-input" id="search_input" placeholder="Search..." type="search"><br>
			</form>
		</div>

		<?php include 'php/list_games.php';?>
	</body>
</html>
